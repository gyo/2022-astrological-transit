import * as モデル from "./モデル";

import type * as 関数の型 from "./関数.型";

export const 各n惑星の配置と各t惑星の各月の配置から各t惑星が各n惑星に対して取る各月のアスペクトを取得: 関数の型.各n惑星の配置と各t惑星の各月の配置から各t惑星が各n惑星に対して取る各月のアスペクトを取得 =
  (
    各n惑星の配置,
    各t惑星の各月の配置,
    各t惑星が各n惑星に対して取る各月のアスペクトを取得,
    特定のt惑星が各n惑星に対して取る各月のアスペクトを取得,
    特定のt惑星が特定のn惑星に対して取る各月のアスペクトを取得,
    特定のt惑星が特定のn惑星に対してコンジャンクションとなる際のアスペクトを取得,
    特定のt惑星が特定のn惑星に対してオポジションとなる際のアスペクトを取得,
    特定のt惑星が特定のn惑星に対してトラインとなる際のアスペクトを取得,
    特定のt惑星が特定のn惑星に対してスクエアとなる際のアスペクトを取得,
    特定のt惑星が特定のn惑星に対してセキスタイルとなる際のアスペクトを取得,
    特定のt惑星が特定のn惑星のアスペクトの範囲内に入るかどうかを取得,
    各n惑星がアスペクトを取りうる範囲を取得,
    特定のn惑星がアスペクトを取りうる範囲を取得,
    特定のn惑星のコンジャンクションとなる範囲を取得,
    特定のn惑星のオポジションとなる範囲を取得,
    特定のn惑星のトラインとなる範囲を取得,
    特定のn惑星のスクエアとなる範囲を取得,
    特定のn惑星のセキスタイルとなる範囲を取得,
    絶対分数を取得,
    絶対分数を正規化,
    分数間の距離を取得,
    要素数が12である
  ) => {
    return 各t惑星が各n惑星に対して取る各月のアスペクトを取得(
      各n惑星がアスペクトを取りうる範囲を取得(
        各n惑星の配置,
        特定のn惑星がアスペクトを取りうる範囲を取得,
        特定のn惑星のコンジャンクションとなる範囲を取得,
        特定のn惑星のオポジションとなる範囲を取得,
        特定のn惑星のトラインとなる範囲を取得,
        特定のn惑星のスクエアとなる範囲を取得,
        特定のn惑星のセキスタイルとなる範囲を取得,
        絶対分数を取得,
        絶対分数を正規化
      ),
      各t惑星の各月の配置,
      特定のt惑星が各n惑星に対して取る各月のアスペクトを取得,
      特定のt惑星が特定のn惑星に対して取る各月のアスペクトを取得,
      特定のt惑星が特定のn惑星に対してコンジャンクションとなる際のアスペクトを取得,
      特定のt惑星が特定のn惑星に対してオポジションとなる際のアスペクトを取得,
      特定のt惑星が特定のn惑星に対してトラインとなる際のアスペクトを取得,
      特定のt惑星が特定のn惑星に対してスクエアとなる際のアスペクトを取得,
      特定のt惑星が特定のn惑星に対してセキスタイルとなる際のアスペクトを取得,
      特定のt惑星が特定のn惑星のアスペクトの範囲内に入るかどうかを取得,
      絶対分数を取得,
      絶対分数を正規化,
      分数間の距離を取得,
      要素数が12である
    );
  };

export const 各t惑星が各n惑星に対して取る各月のアスペクトを取得: 関数の型.各t惑星が各n惑星に対して取る各月のアスペクトを取得 =
  (
    各n惑星がアスペクトを取りうる範囲,
    各t惑星の各月の配置,
    特定のt惑星が各n惑星に対して取る各月のアスペクトを取得,
    特定のt惑星が特定のn惑星に対して取る各月のアスペクトを取得,
    特定のt惑星が特定のn惑星に対してコンジャンクションとなる際のアスペクトを取得,
    特定のt惑星が特定のn惑星に対してオポジションとなる際のアスペクトを取得,
    特定のt惑星が特定のn惑星に対してトラインとなる際のアスペクトを取得,
    特定のt惑星が特定のn惑星に対してスクエアとなる際のアスペクトを取得,
    特定のt惑星が特定のn惑星に対してセキスタイルとなる際のアスペクトを取得,
    特定のt惑星が特定のn惑星のアスペクトの範囲内に入るかどうかを取得,
    絶対分数を取得,
    絶対分数を正規化,
    分数間の距離を取得,
    要素数が12である
  ) => {
    return {
      冥王星: 特定のt惑星が各n惑星に対して取る各月のアスペクトを取得(
        各t惑星の各月の配置.冥王星,
        各n惑星がアスペクトを取りうる範囲,
        特定のt惑星が特定のn惑星に対して取る各月のアスペクトを取得,
        特定のt惑星が特定のn惑星に対してコンジャンクションとなる際のアスペクトを取得,
        特定のt惑星が特定のn惑星に対してオポジションとなる際のアスペクトを取得,
        特定のt惑星が特定のn惑星に対してトラインとなる際のアスペクトを取得,
        特定のt惑星が特定のn惑星に対してスクエアとなる際のアスペクトを取得,
        特定のt惑星が特定のn惑星に対してセキスタイルとなる際のアスペクトを取得,
        特定のt惑星が特定のn惑星のアスペクトの範囲内に入るかどうかを取得,
        絶対分数を取得,
        絶対分数を正規化,
        分数間の距離を取得,
        要素数が12である
      ),
      海王星: 特定のt惑星が各n惑星に対して取る各月のアスペクトを取得(
        各t惑星の各月の配置.海王星,
        各n惑星がアスペクトを取りうる範囲,
        特定のt惑星が特定のn惑星に対して取る各月のアスペクトを取得,
        特定のt惑星が特定のn惑星に対してコンジャンクションとなる際のアスペクトを取得,
        特定のt惑星が特定のn惑星に対してオポジションとなる際のアスペクトを取得,
        特定のt惑星が特定のn惑星に対してトラインとなる際のアスペクトを取得,
        特定のt惑星が特定のn惑星に対してスクエアとなる際のアスペクトを取得,
        特定のt惑星が特定のn惑星に対してセキスタイルとなる際のアスペクトを取得,
        特定のt惑星が特定のn惑星のアスペクトの範囲内に入るかどうかを取得,
        絶対分数を取得,
        絶対分数を正規化,
        分数間の距離を取得,
        要素数が12である
      ),
      天王星: 特定のt惑星が各n惑星に対して取る各月のアスペクトを取得(
        各t惑星の各月の配置.天王星,
        各n惑星がアスペクトを取りうる範囲,
        特定のt惑星が特定のn惑星に対して取る各月のアスペクトを取得,
        特定のt惑星が特定のn惑星に対してコンジャンクションとなる際のアスペクトを取得,
        特定のt惑星が特定のn惑星に対してオポジションとなる際のアスペクトを取得,
        特定のt惑星が特定のn惑星に対してトラインとなる際のアスペクトを取得,
        特定のt惑星が特定のn惑星に対してスクエアとなる際のアスペクトを取得,
        特定のt惑星が特定のn惑星に対してセキスタイルとなる際のアスペクトを取得,
        特定のt惑星が特定のn惑星のアスペクトの範囲内に入るかどうかを取得,
        絶対分数を取得,
        絶対分数を正規化,
        分数間の距離を取得,
        要素数が12である
      ),
      土星: 特定のt惑星が各n惑星に対して取る各月のアスペクトを取得(
        各t惑星の各月の配置.土星,
        各n惑星がアスペクトを取りうる範囲,
        特定のt惑星が特定のn惑星に対して取る各月のアスペクトを取得,
        特定のt惑星が特定のn惑星に対してコンジャンクションとなる際のアスペクトを取得,
        特定のt惑星が特定のn惑星に対してオポジションとなる際のアスペクトを取得,
        特定のt惑星が特定のn惑星に対してトラインとなる際のアスペクトを取得,
        特定のt惑星が特定のn惑星に対してスクエアとなる際のアスペクトを取得,
        特定のt惑星が特定のn惑星に対してセキスタイルとなる際のアスペクトを取得,
        特定のt惑星が特定のn惑星のアスペクトの範囲内に入るかどうかを取得,
        絶対分数を取得,
        絶対分数を正規化,
        分数間の距離を取得,
        要素数が12である
      ),
      木星: 特定のt惑星が各n惑星に対して取る各月のアスペクトを取得(
        各t惑星の各月の配置.木星,
        各n惑星がアスペクトを取りうる範囲,
        特定のt惑星が特定のn惑星に対して取る各月のアスペクトを取得,
        特定のt惑星が特定のn惑星に対してコンジャンクションとなる際のアスペクトを取得,
        特定のt惑星が特定のn惑星に対してオポジションとなる際のアスペクトを取得,
        特定のt惑星が特定のn惑星に対してトラインとなる際のアスペクトを取得,
        特定のt惑星が特定のn惑星に対してスクエアとなる際のアスペクトを取得,
        特定のt惑星が特定のn惑星に対してセキスタイルとなる際のアスペクトを取得,
        特定のt惑星が特定のn惑星のアスペクトの範囲内に入るかどうかを取得,
        絶対分数を取得,
        絶対分数を正規化,
        分数間の距離を取得,
        要素数が12である
      ),
    };
  };

export const 特定のt惑星が各n惑星に対して取る各月のアスペクトを取得: 関数の型.特定のt惑星が各n惑星に対して取る各月のアスペクトを取得 =
  (
    特定のt惑星の各月の配置,
    各n惑星がアスペクトを取りうる範囲,
    特定のt惑星が特定のn惑星に対して取る各月のアスペクトを取得,
    特定のt惑星が特定のn惑星に対してコンジャンクションとなる際のアスペクトを取得,
    特定のt惑星が特定のn惑星に対してオポジションとなる際のアスペクトを取得,
    特定のt惑星が特定のn惑星に対してトラインとなる際のアスペクトを取得,
    特定のt惑星が特定のn惑星に対してスクエアとなる際のアスペクトを取得,
    特定のt惑星が特定のn惑星に対してセキスタイルとなる際のアスペクトを取得,
    特定のt惑星が特定のn惑星のアスペクトの範囲内に入るかどうかを取得,
    絶対分数を取得,
    絶対分数を正規化,
    分数間の距離を取得,
    要素数が12である
  ) => {
    return {
      太陽: 特定のt惑星が特定のn惑星に対して取る各月のアスペクトを取得(
        特定のt惑星の各月の配置,
        各n惑星がアスペクトを取りうる範囲.太陽,
        特定のt惑星が特定のn惑星に対してコンジャンクションとなる際のアスペクトを取得,
        特定のt惑星が特定のn惑星に対してオポジションとなる際のアスペクトを取得,
        特定のt惑星が特定のn惑星に対してトラインとなる際のアスペクトを取得,
        特定のt惑星が特定のn惑星に対してスクエアとなる際のアスペクトを取得,
        特定のt惑星が特定のn惑星に対してセキスタイルとなる際のアスペクトを取得,
        特定のt惑星が特定のn惑星のアスペクトの範囲内に入るかどうかを取得,
        絶対分数を取得,
        絶対分数を正規化,
        分数間の距離を取得,
        要素数が12である
      ),
      月: 特定のt惑星が特定のn惑星に対して取る各月のアスペクトを取得(
        特定のt惑星の各月の配置,
        各n惑星がアスペクトを取りうる範囲.月,
        特定のt惑星が特定のn惑星に対してコンジャンクションとなる際のアスペクトを取得,
        特定のt惑星が特定のn惑星に対してオポジションとなる際のアスペクトを取得,
        特定のt惑星が特定のn惑星に対してトラインとなる際のアスペクトを取得,
        特定のt惑星が特定のn惑星に対してスクエアとなる際のアスペクトを取得,
        特定のt惑星が特定のn惑星に対してセキスタイルとなる際のアスペクトを取得,
        特定のt惑星が特定のn惑星のアスペクトの範囲内に入るかどうかを取得,
        絶対分数を取得,
        絶対分数を正規化,
        分数間の距離を取得,
        要素数が12である
      ),
      水星: 特定のt惑星が特定のn惑星に対して取る各月のアスペクトを取得(
        特定のt惑星の各月の配置,
        各n惑星がアスペクトを取りうる範囲.水星,
        特定のt惑星が特定のn惑星に対してコンジャンクションとなる際のアスペクトを取得,
        特定のt惑星が特定のn惑星に対してオポジションとなる際のアスペクトを取得,
        特定のt惑星が特定のn惑星に対してトラインとなる際のアスペクトを取得,
        特定のt惑星が特定のn惑星に対してスクエアとなる際のアスペクトを取得,
        特定のt惑星が特定のn惑星に対してセキスタイルとなる際のアスペクトを取得,
        特定のt惑星が特定のn惑星のアスペクトの範囲内に入るかどうかを取得,
        絶対分数を取得,
        絶対分数を正規化,
        分数間の距離を取得,
        要素数が12である
      ),
      金星: 特定のt惑星が特定のn惑星に対して取る各月のアスペクトを取得(
        特定のt惑星の各月の配置,
        各n惑星がアスペクトを取りうる範囲.金星,
        特定のt惑星が特定のn惑星に対してコンジャンクションとなる際のアスペクトを取得,
        特定のt惑星が特定のn惑星に対してオポジションとなる際のアスペクトを取得,
        特定のt惑星が特定のn惑星に対してトラインとなる際のアスペクトを取得,
        特定のt惑星が特定のn惑星に対してスクエアとなる際のアスペクトを取得,
        特定のt惑星が特定のn惑星に対してセキスタイルとなる際のアスペクトを取得,
        特定のt惑星が特定のn惑星のアスペクトの範囲内に入るかどうかを取得,
        絶対分数を取得,
        絶対分数を正規化,
        分数間の距離を取得,
        要素数が12である
      ),
      火星: 特定のt惑星が特定のn惑星に対して取る各月のアスペクトを取得(
        特定のt惑星の各月の配置,
        各n惑星がアスペクトを取りうる範囲.火星,
        特定のt惑星が特定のn惑星に対してコンジャンクションとなる際のアスペクトを取得,
        特定のt惑星が特定のn惑星に対してオポジションとなる際のアスペクトを取得,
        特定のt惑星が特定のn惑星に対してトラインとなる際のアスペクトを取得,
        特定のt惑星が特定のn惑星に対してスクエアとなる際のアスペクトを取得,
        特定のt惑星が特定のn惑星に対してセキスタイルとなる際のアスペクトを取得,
        特定のt惑星が特定のn惑星のアスペクトの範囲内に入るかどうかを取得,
        絶対分数を取得,
        絶対分数を正規化,
        分数間の距離を取得,
        要素数が12である
      ),
      木星: 特定のt惑星が特定のn惑星に対して取る各月のアスペクトを取得(
        特定のt惑星の各月の配置,
        各n惑星がアスペクトを取りうる範囲.木星,
        特定のt惑星が特定のn惑星に対してコンジャンクションとなる際のアスペクトを取得,
        特定のt惑星が特定のn惑星に対してオポジションとなる際のアスペクトを取得,
        特定のt惑星が特定のn惑星に対してトラインとなる際のアスペクトを取得,
        特定のt惑星が特定のn惑星に対してスクエアとなる際のアスペクトを取得,
        特定のt惑星が特定のn惑星に対してセキスタイルとなる際のアスペクトを取得,
        特定のt惑星が特定のn惑星のアスペクトの範囲内に入るかどうかを取得,
        絶対分数を取得,
        絶対分数を正規化,
        分数間の距離を取得,
        要素数が12である
      ),
      土星: 特定のt惑星が特定のn惑星に対して取る各月のアスペクトを取得(
        特定のt惑星の各月の配置,
        各n惑星がアスペクトを取りうる範囲.土星,
        特定のt惑星が特定のn惑星に対してコンジャンクションとなる際のアスペクトを取得,
        特定のt惑星が特定のn惑星に対してオポジションとなる際のアスペクトを取得,
        特定のt惑星が特定のn惑星に対してトラインとなる際のアスペクトを取得,
        特定のt惑星が特定のn惑星に対してスクエアとなる際のアスペクトを取得,
        特定のt惑星が特定のn惑星に対してセキスタイルとなる際のアスペクトを取得,
        特定のt惑星が特定のn惑星のアスペクトの範囲内に入るかどうかを取得,
        絶対分数を取得,
        絶対分数を正規化,
        分数間の距離を取得,
        要素数が12である
      ),
    };
  };

export const 特定のt惑星が特定のn惑星に対して取る各月のアスペクトを取得: 関数の型.特定のt惑星が特定のn惑星に対して取る各月のアスペクトを取得 =
  (
    特定のt惑星の各月の配置,
    特定のn惑星がアスペクトを取りうる範囲,
    特定のt惑星が特定のn惑星に対してコンジャンクションとなる際のアスペクトを取得,
    特定のt惑星が特定のn惑星に対してオポジションとなる際のアスペクトを取得,
    特定のt惑星が特定のn惑星に対してトラインとなる際のアスペクトを取得,
    特定のt惑星が特定のn惑星に対してスクエアとなる際のアスペクトを取得,
    特定のt惑星が特定のn惑星に対してセキスタイルとなる際のアスペクトを取得,
    特定のt惑星が特定のn惑星のアスペクトの範囲内に入るかどうかを取得,
    絶対分数を取得,
    絶対分数を正規化,
    分数間の距離を取得,
    要素数が12である
  ) => {
    const 結果 = 特定のt惑星の各月の配置.map((特定のt惑星の特定の月の配置) => {
      const コンジャンクションとなる際のアスペクト =
        特定のt惑星が特定のn惑星に対してコンジャンクションとなる際のアスペクトを取得(
          特定のt惑星の特定の月の配置,
          特定のn惑星がアスペクトを取りうる範囲,
          特定のt惑星が特定のn惑星のアスペクトの範囲内に入るかどうかを取得,
          絶対分数を取得,
          絶対分数を正規化,
          分数間の距離を取得
        );
      if (コンジャンクションとなる際のアスペクト != null) {
        return コンジャンクションとなる際のアスペクト;
      }

      const オポジションとなる際のアスペクト =
        特定のt惑星が特定のn惑星に対してオポジションとなる際のアスペクトを取得(
          特定のt惑星の特定の月の配置,
          特定のn惑星がアスペクトを取りうる範囲,
          特定のt惑星が特定のn惑星のアスペクトの範囲内に入るかどうかを取得,
          絶対分数を取得,
          絶対分数を正規化,
          分数間の距離を取得
        );
      if (オポジションとなる際のアスペクト != null) {
        return オポジションとなる際のアスペクト;
      }

      const トラインとなる際のアスペクト =
        特定のt惑星が特定のn惑星に対してトラインとなる際のアスペクトを取得(
          特定のt惑星の特定の月の配置,
          特定のn惑星がアスペクトを取りうる範囲,
          特定のt惑星が特定のn惑星のアスペクトの範囲内に入るかどうかを取得,
          絶対分数を取得,
          絶対分数を正規化,
          分数間の距離を取得
        );
      if (トラインとなる際のアスペクト != null) {
        return トラインとなる際のアスペクト;
      }

      const スクエアとなる際のアスペクト =
        特定のt惑星が特定のn惑星に対してスクエアとなる際のアスペクトを取得(
          特定のt惑星の特定の月の配置,
          特定のn惑星がアスペクトを取りうる範囲,
          特定のt惑星が特定のn惑星のアスペクトの範囲内に入るかどうかを取得,
          絶対分数を取得,
          絶対分数を正規化,
          分数間の距離を取得
        );
      if (スクエアとなる際のアスペクト != null) {
        return スクエアとなる際のアスペクト;
      }

      const セキスタイルとなる際のアスペクト =
        特定のt惑星が特定のn惑星に対してセキスタイルとなる際のアスペクトを取得(
          特定のt惑星の特定の月の配置,
          特定のn惑星がアスペクトを取りうる範囲,
          特定のt惑星が特定のn惑星のアスペクトの範囲内に入るかどうかを取得,
          絶対分数を取得,
          絶対分数を正規化,
          分数間の距離を取得
        );
      if (セキスタイルとなる際のアスペクト != null) {
        return セキスタイルとなる際のアスペクト;
      }

      return;
    });

    if (要素数が12である(結果)) {
      return 結果;
    }

    throw new Error("関数 要素数が12である が false を返しました");
  };

export const 特定のt惑星が特定のn惑星に対してコンジャンクションとなる際のアスペクトを取得: 関数の型.特定のt惑星が特定のn惑星に対してコンジャンクションとなる際のアスペクトを取得 =
  (
    特定のt惑星の配置,
    特定のn惑星がアスペクトを取りうる範囲,
    特定のt惑星が特定のn惑星のアスペクトの範囲内に入るかどうかを取得,
    絶対分数を取得,
    絶対分数を正規化,
    分数間の距離を取得
  ) => {
    const 特定のt惑星の絶対分数 = 絶対分数を取得(
      特定のt惑星の配置,
      絶対分数を正規化
    );
    const 特定のn惑星のアスペクトの範囲 =
      特定のn惑星がアスペクトを取りうる範囲["コンジャンクション"][0];

    if (
      特定のt惑星が特定のn惑星のアスペクトの範囲内に入るかどうかを取得(
        特定のt惑星の絶対分数,
        特定のn惑星のアスペクトの範囲
      )
    ) {
      return {
        アスペクト: "コンジャンクション",
        分数差: 分数間の距離を取得(
          特定のt惑星の絶対分数,
          特定のn惑星のアスペクトの範囲.正確な値
        ),
      };
    }

    return;
  };

export const 特定のt惑星が特定のn惑星に対してオポジションとなる際のアスペクトを取得: 関数の型.特定のt惑星が特定のn惑星に対してオポジションとなる際のアスペクトを取得 =
  (
    特定のt惑星の配置,
    特定のn惑星がアスペクトを取りうる範囲,
    特定のt惑星が特定のn惑星のアスペクトの範囲内に入るかどうかを取得,
    絶対分数を取得,
    絶対分数を正規化,
    分数間の距離を取得
  ) => {
    const 特定のt惑星の絶対分数 = 絶対分数を取得(
      特定のt惑星の配置,
      絶対分数を正規化
    );
    const 特定のn惑星のアスペクトの範囲 =
      特定のn惑星がアスペクトを取りうる範囲["オポジション"][0];

    if (
      特定のt惑星が特定のn惑星のアスペクトの範囲内に入るかどうかを取得(
        特定のt惑星の絶対分数,
        特定のn惑星のアスペクトの範囲
      )
    ) {
      return {
        アスペクト: "オポジション",
        分数差: 分数間の距離を取得(
          特定のt惑星の絶対分数,
          特定のn惑星のアスペクトの範囲.正確な値
        ),
      };
    }

    return;
  };

export const 特定のt惑星が特定のn惑星に対してトラインとなる際のアスペクトを取得: 関数の型.特定のt惑星が特定のn惑星に対してトラインとなる際のアスペクトを取得 =
  (
    特定のt惑星の配置,
    特定のn惑星がアスペクトを取りうる範囲,
    特定のt惑星が特定のn惑星のアスペクトの範囲内に入るかどうかを取得,
    絶対分数を取得,
    絶対分数を正規化,
    分数間の距離を取得
  ) => {
    const 特定のt惑星の絶対分数 = 絶対分数を取得(
      特定のt惑星の配置,
      絶対分数を正規化
    );
    const 特定のn惑星のアスペクトの範囲1 =
      特定のn惑星がアスペクトを取りうる範囲["トライン"][0];
    const 特定のn惑星のアスペクトの範囲2 =
      特定のn惑星がアスペクトを取りうる範囲["トライン"][1];

    if (
      特定のt惑星が特定のn惑星のアスペクトの範囲内に入るかどうかを取得(
        特定のt惑星の絶対分数,
        特定のn惑星のアスペクトの範囲1
      )
    ) {
      return {
        アスペクト: "トライン",
        分数差: 分数間の距離を取得(
          特定のt惑星の絶対分数,
          特定のn惑星のアスペクトの範囲1.正確な値
        ),
      };
    }

    if (
      特定のt惑星が特定のn惑星のアスペクトの範囲内に入るかどうかを取得(
        特定のt惑星の絶対分数,
        特定のn惑星のアスペクトの範囲2
      )
    ) {
      return {
        アスペクト: "トライン",
        分数差: 分数間の距離を取得(
          特定のt惑星の絶対分数,
          特定のn惑星のアスペクトの範囲2.正確な値
        ),
      };
    }

    return;
  };

export const 特定のt惑星が特定のn惑星に対してスクエアとなる際のアスペクトを取得: 関数の型.特定のt惑星が特定のn惑星に対してスクエアとなる際のアスペクトを取得 =
  (
    特定のt惑星の配置,
    特定のn惑星がアスペクトを取りうる範囲,
    特定のt惑星が特定のn惑星のアスペクトの範囲内に入るかどうかを取得,
    絶対分数を取得,
    絶対分数を正規化,
    分数間の距離を取得
  ) => {
    const 特定のt惑星の絶対分数 = 絶対分数を取得(
      特定のt惑星の配置,
      絶対分数を正規化
    );
    const 特定のn惑星のアスペクトの範囲1 =
      特定のn惑星がアスペクトを取りうる範囲["スクエア"][0];
    const 特定のn惑星のアスペクトの範囲2 =
      特定のn惑星がアスペクトを取りうる範囲["スクエア"][1];

    if (
      特定のt惑星が特定のn惑星のアスペクトの範囲内に入るかどうかを取得(
        特定のt惑星の絶対分数,
        特定のn惑星のアスペクトの範囲1
      )
    ) {
      return {
        アスペクト: "スクエア",
        分数差: 分数間の距離を取得(
          特定のt惑星の絶対分数,
          特定のn惑星のアスペクトの範囲1.正確な値
        ),
      };
    }

    if (
      特定のt惑星が特定のn惑星のアスペクトの範囲内に入るかどうかを取得(
        特定のt惑星の絶対分数,
        特定のn惑星のアスペクトの範囲2
      )
    ) {
      return {
        アスペクト: "スクエア",
        分数差: 分数間の距離を取得(
          特定のt惑星の絶対分数,
          特定のn惑星のアスペクトの範囲2.正確な値
        ),
      };
    }

    return;
  };

export const 特定のt惑星が特定のn惑星に対してセキスタイルとなる際のアスペクトを取得: 関数の型.特定のt惑星が特定のn惑星に対してセキスタイルとなる際のアスペクトを取得 =
  (
    特定のt惑星の配置,
    特定のn惑星がアスペクトを取りうる範囲,
    特定のt惑星が特定のn惑星のアスペクトの範囲内に入るかどうかを取得,
    絶対分数を取得,
    絶対分数を正規化,
    分数間の距離を取得
  ) => {
    const 特定のt惑星の絶対分数 = 絶対分数を取得(
      特定のt惑星の配置,
      絶対分数を正規化
    );
    const 特定のn惑星のアスペクトの範囲1 =
      特定のn惑星がアスペクトを取りうる範囲["セキスタイル"][0];
    const 特定のn惑星のアスペクトの範囲2 =
      特定のn惑星がアスペクトを取りうる範囲["セキスタイル"][1];

    if (
      特定のt惑星が特定のn惑星のアスペクトの範囲内に入るかどうかを取得(
        特定のt惑星の絶対分数,
        特定のn惑星のアスペクトの範囲1
      )
    ) {
      return {
        アスペクト: "セキスタイル",
        分数差: 分数間の距離を取得(
          特定のt惑星の絶対分数,
          特定のn惑星のアスペクトの範囲1.正確な値
        ),
      };
    }

    if (
      特定のt惑星が特定のn惑星のアスペクトの範囲内に入るかどうかを取得(
        特定のt惑星の絶対分数,
        特定のn惑星のアスペクトの範囲2
      )
    ) {
      return {
        アスペクト: "セキスタイル",
        分数差: 分数間の距離を取得(
          特定のt惑星の絶対分数,
          特定のn惑星のアスペクトの範囲2.正確な値
        ),
      };
    }

    return;
  };

export const 特定のt惑星が特定のn惑星のアスペクトの範囲内に入るかどうかを取得: 関数の型.特定のt惑星が特定のn惑星のアスペクトの範囲内に入るかどうかを取得 =
  (
    特定のt惑星の絶対分数: モデル.絶対分数,
    特定のn惑星のアスペクトの範囲: モデル.アスペクトを取りうる範囲
  ) => {
    return (
      // 始点の値が終点の値以下場合、始点以上かつ終点以下の値であれば真となる
      (特定のn惑星のアスペクトの範囲.始点 <=
        特定のn惑星のアスペクトの範囲.終点 &&
        特定のn惑星のアスペクトの範囲.始点 <= 特定のt惑星の絶対分数 &&
        特定のt惑星の絶対分数 <= 特定のn惑星のアスペクトの範囲.終点) ||
      // 始点の値が終点の値よりも大きい場合、始点以上または終点以下の値であれば真となる
      (特定のn惑星のアスペクトの範囲.終点 <
        特定のn惑星のアスペクトの範囲.始点 &&
        (特定のn惑星のアスペクトの範囲.始点 <= 特定のt惑星の絶対分数 ||
          特定のt惑星の絶対分数 <= 特定のn惑星のアスペクトの範囲.終点))
    );
  };

export const 各n惑星がアスペクトを取りうる範囲を取得: 関数の型.各n惑星がアスペクトを取りうる範囲を取得 =
  (
    各n惑星の配置,
    特定のn惑星がアスペクトを取りうる範囲を取得,
    特定のn惑星のコンジャンクションとなる範囲を取得,
    特定のn惑星のオポジションとなる範囲を取得,
    特定のn惑星のトラインとなる範囲を取得,
    特定のn惑星のスクエアとなる範囲を取得,
    特定のn惑星のセキスタイルとなる範囲を取得,
    絶対分数を取得,
    絶対分数を正規化
  ) => {
    return {
      太陽: 特定のn惑星がアスペクトを取りうる範囲を取得(
        各n惑星の配置.太陽,
        特定のn惑星のコンジャンクションとなる範囲を取得,
        特定のn惑星のオポジションとなる範囲を取得,
        特定のn惑星のトラインとなる範囲を取得,
        特定のn惑星のスクエアとなる範囲を取得,
        特定のn惑星のセキスタイルとなる範囲を取得,
        絶対分数を取得,
        絶対分数を正規化
      ),
      月: 特定のn惑星がアスペクトを取りうる範囲を取得(
        各n惑星の配置.月,
        特定のn惑星のコンジャンクションとなる範囲を取得,
        特定のn惑星のオポジションとなる範囲を取得,
        特定のn惑星のトラインとなる範囲を取得,
        特定のn惑星のスクエアとなる範囲を取得,
        特定のn惑星のセキスタイルとなる範囲を取得,
        絶対分数を取得,
        絶対分数を正規化
      ),
      水星: 特定のn惑星がアスペクトを取りうる範囲を取得(
        各n惑星の配置.水星,
        特定のn惑星のコンジャンクションとなる範囲を取得,
        特定のn惑星のオポジションとなる範囲を取得,
        特定のn惑星のトラインとなる範囲を取得,
        特定のn惑星のスクエアとなる範囲を取得,
        特定のn惑星のセキスタイルとなる範囲を取得,
        絶対分数を取得,
        絶対分数を正規化
      ),
      金星: 特定のn惑星がアスペクトを取りうる範囲を取得(
        各n惑星の配置.金星,
        特定のn惑星のコンジャンクションとなる範囲を取得,
        特定のn惑星のオポジションとなる範囲を取得,
        特定のn惑星のトラインとなる範囲を取得,
        特定のn惑星のスクエアとなる範囲を取得,
        特定のn惑星のセキスタイルとなる範囲を取得,
        絶対分数を取得,
        絶対分数を正規化
      ),
      火星: 特定のn惑星がアスペクトを取りうる範囲を取得(
        各n惑星の配置.火星,
        特定のn惑星のコンジャンクションとなる範囲を取得,
        特定のn惑星のオポジションとなる範囲を取得,
        特定のn惑星のトラインとなる範囲を取得,
        特定のn惑星のスクエアとなる範囲を取得,
        特定のn惑星のセキスタイルとなる範囲を取得,
        絶対分数を取得,
        絶対分数を正規化
      ),
      木星: 特定のn惑星がアスペクトを取りうる範囲を取得(
        各n惑星の配置.木星,
        特定のn惑星のコンジャンクションとなる範囲を取得,
        特定のn惑星のオポジションとなる範囲を取得,
        特定のn惑星のトラインとなる範囲を取得,
        特定のn惑星のスクエアとなる範囲を取得,
        特定のn惑星のセキスタイルとなる範囲を取得,
        絶対分数を取得,
        絶対分数を正規化
      ),
      土星: 特定のn惑星がアスペクトを取りうる範囲を取得(
        各n惑星の配置.土星,
        特定のn惑星のコンジャンクションとなる範囲を取得,
        特定のn惑星のオポジションとなる範囲を取得,
        特定のn惑星のトラインとなる範囲を取得,
        特定のn惑星のスクエアとなる範囲を取得,
        特定のn惑星のセキスタイルとなる範囲を取得,
        絶対分数を取得,
        絶対分数を正規化
      ),
    };
  };

export const 特定のn惑星がアスペクトを取りうる範囲を取得: 関数の型.特定のn惑星がアスペクトを取りうる範囲を取得 =
  (
    特定のn惑星の配置,
    特定のn惑星のコンジャンクションとなる範囲を取得,
    特定のn惑星のオポジションとなる範囲を取得,
    特定のn惑星のトラインとなる範囲を取得,
    特定のn惑星のスクエアとなる範囲を取得,
    特定のn惑星のセキスタイルとなる範囲を取得,
    絶対分数を取得,
    絶対分数を正規化
  ) => {
    const 特定のn惑星の絶対分数 = 絶対分数を取得(
      特定のn惑星の配置,
      絶対分数を正規化
    );

    return {
      コンジャンクション: 特定のn惑星のコンジャンクションとなる範囲を取得(
        特定のn惑星の絶対分数,
        絶対分数を正規化
      ),
      オポジション: 特定のn惑星のオポジションとなる範囲を取得(
        特定のn惑星の絶対分数,
        絶対分数を正規化
      ),
      トライン: 特定のn惑星のトラインとなる範囲を取得(
        特定のn惑星の絶対分数,
        絶対分数を正規化
      ),
      スクエア: 特定のn惑星のスクエアとなる範囲を取得(
        特定のn惑星の絶対分数,
        絶対分数を正規化
      ),
      セキスタイル: 特定のn惑星のセキスタイルとなる範囲を取得(
        特定のn惑星の絶対分数,
        絶対分数を正規化
      ),
    };
  };

/**
 * 返り値の "始点" は "終点" よりも大きい数値をとることがある
 */
export const 特定のn惑星のコンジャンクションとなる範囲を取得: 関数の型.特定のn惑星のコンジャンクションとなる範囲を取得 =
  (絶対分数, 絶対分数を正規化) => {
    const アスペクトの分数 = モデル.アスペクトごとの分数["コンジャンクション"];
    const アスペクトのオーブ =
      モデル.アスペクトごとのオーブの分数["コンジャンクション"];
    return [
      {
        始点: 絶対分数を正規化(
          絶対分数 + アスペクトの分数[0] - アスペクトのオーブ
        ),
        正確な値: 絶対分数を正規化(絶対分数 + アスペクトの分数[0]),
        終点: 絶対分数を正規化(
          絶対分数 + アスペクトの分数[0] + アスペクトのオーブ
        ),
      },
    ];
  };

/**
 * 返り値の "始点" は "終点" よりも大きい数値をとることがある
 */
export const 特定のn惑星のオポジションとなる範囲を取得: 関数の型.特定のn惑星のオポジションとなる範囲を取得 =
  (絶対分数, 絶対分数を正規化) => {
    const アスペクトの分数 = モデル.アスペクトごとの分数["オポジション"];
    const アスペクトのオーブ =
      モデル.アスペクトごとのオーブの分数["オポジション"];
    return [
      {
        始点: 絶対分数を正規化(
          絶対分数 + アスペクトの分数[0] - アスペクトのオーブ
        ),
        正確な値: 絶対分数を正規化(絶対分数 + アスペクトの分数[0]),
        終点: 絶対分数を正規化(
          絶対分数 + アスペクトの分数[0] + アスペクトのオーブ
        ),
      },
    ];
  };

/**
 * 返り値の "始点" は "終点" よりも大きい数値をとることがある
 */
export const 特定のn惑星のトラインとなる範囲を取得: 関数の型.特定のn惑星のトラインとなる範囲を取得 =
  (絶対分数, 絶対分数を正規化) => {
    const アスペクトの分数 = モデル.アスペクトごとの分数["トライン"];
    const アスペクトのオーブ = モデル.アスペクトごとのオーブの分数["トライン"];
    return [
      {
        始点: 絶対分数を正規化(
          絶対分数 + アスペクトの分数[0] - アスペクトのオーブ
        ),
        正確な値: 絶対分数を正規化(絶対分数 + アスペクトの分数[0]),
        終点: 絶対分数を正規化(
          絶対分数 + アスペクトの分数[0] + アスペクトのオーブ
        ),
      },
      {
        始点: 絶対分数を正規化(
          絶対分数 + アスペクトの分数[1] - アスペクトのオーブ
        ),
        正確な値: 絶対分数を正規化(絶対分数 + アスペクトの分数[1]),
        終点: 絶対分数を正規化(
          絶対分数 + アスペクトの分数[1] + アスペクトのオーブ
        ),
      },
    ];
  };

/**
 * 返り値の "始点" は "終点" よりも大きい数値をとることがある
 */
export const 特定のn惑星のスクエアとなる範囲を取得: 関数の型.特定のn惑星のスクエアとなる範囲を取得 =
  (絶対分数, 絶対分数を正規化) => {
    const アスペクトの分数 = モデル.アスペクトごとの分数["スクエア"];
    const アスペクトのオーブ = モデル.アスペクトごとのオーブの分数["スクエア"];
    return [
      {
        始点: 絶対分数を正規化(
          絶対分数 + アスペクトの分数[0] - アスペクトのオーブ
        ),
        正確な値: 絶対分数を正規化(絶対分数 + アスペクトの分数[0]),
        終点: 絶対分数を正規化(
          絶対分数 + アスペクトの分数[0] + アスペクトのオーブ
        ),
      },
      {
        始点: 絶対分数を正規化(
          絶対分数 + アスペクトの分数[1] - アスペクトのオーブ
        ),
        正確な値: 絶対分数を正規化(絶対分数 + アスペクトの分数[1]),
        終点: 絶対分数を正規化(
          絶対分数 + アスペクトの分数[1] + アスペクトのオーブ
        ),
      },
    ];
  };

/**
 * 返り値の "始点" は "終点" よりも大きい数値をとることがある
 */
export const 特定のn惑星のセキスタイルとなる範囲を取得: 関数の型.特定のn惑星のセキスタイルとなる範囲を取得 =
  (絶対分数, 絶対分数を正規化) => {
    const アスペクトの分数 = モデル.アスペクトごとの分数["セキスタイル"];
    const アスペクトのオーブ =
      モデル.アスペクトごとのオーブの分数["セキスタイル"];
    return [
      {
        始点: 絶対分数を正規化(
          絶対分数 + アスペクトの分数[0] - アスペクトのオーブ
        ),
        正確な値: 絶対分数を正規化(絶対分数 + アスペクトの分数[0]),
        終点: 絶対分数を正規化(
          絶対分数 + アスペクトの分数[0] + アスペクトのオーブ
        ),
      },
      {
        始点: 絶対分数を正規化(
          絶対分数 + アスペクトの分数[1] - アスペクトのオーブ
        ),
        正確な値: 絶対分数を正規化(絶対分数 + アスペクトの分数[1]),
        終点: 絶対分数を正規化(
          絶対分数 + アスペクトの分数[1] + アスペクトのオーブ
        ),
      },
    ];
  };

export const 絶対分数を取得: 関数の型.絶対分数を取得 = (
  配置,
  絶対分数を正規化
) => {
  return 絶対分数を正規化(
    モデル.星座ごとのオフセットの分数[配置.星座] + 配置.度 * 60 + 配置.分
  );
};

export const 絶対分数を正規化: 関数の型.絶対分数を正規化 = (絶対分数) => {
  if (絶対分数 < 0) {
    // たとえば -1 は 21599 とする
    return 絶対分数を正規化(モデル.絶対分数の最大値 + 1 + 絶対分数);
  }
  if (モデル.絶対分数の最大値 < 絶対分数) {
    // たとえば 21600 は 0 とする
    return 絶対分数を正規化(絶対分数 - モデル.絶対分数の最大値 - 1);
  }
  return 絶対分数;
};

export const 分数間の距離を取得: 関数の型.分数間の距離を取得 = (
  絶対分数1,
  絶対分数2
) => {
  const 分数差 =
    絶対分数1 < 絶対分数2 ? 絶対分数2 - 絶対分数1 : 絶対分数1 - 絶対分数2;

  // 最大値をまたぐときに数値が大きくなってしまうのを防ぐ
  // たとえば 21599 と 0 の差は 21599 ではなく 1 になるように
  const 対となる分数差 = モデル.絶対分数の最大値 + 1 - 分数差;
  const 距離 = 分数差 < 対となる分数差 ? 分数差 : 対となる分数差;

  return 距離;
};

export const 要素数が12である: 関数の型.要素数が12である = <要素の型>(
  配列: 要素の型[]
): 配列 is [
  要素の型,
  要素の型,
  要素の型,
  要素の型,
  要素の型,
  要素の型,
  要素の型,
  要素の型,
  要素の型,
  要素の型,
  要素の型,
  要素の型
] => {
  return 配列.length === 12;
};

export const アスペクトを表示用フォーマットに変換する: 関数の型.アスペクトを表示用フォーマットに変換する =
  (アスペクト) => {
    if (アスペクト === "コンジャンクション") {
      return "0";
    } else if (アスペクト === "オポジション") {
      return "180";
    } else if (アスペクト === "トライン") {
      return "120";
    } else if (アスペクト === "スクエア") {
      return "90";
    } else if (アスペクト === "セキスタイル") {
      return "60";
    } else {
      throw new Error("アスペクトが不正です");
    }
  };

export const 分数差を表示用フォーマットに変換する: 関数の型.分数差を表示用フォーマットに変換する =
  (分数差) => {
    const 度 = Math.floor(分数差 / 60);
    const 分 = 分数差 % 60;

    return `${度}°${分}′`;
  };
