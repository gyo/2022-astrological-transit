import { GetServerSideProps } from "next";
import Head from "next/head";
import React from "react";

import { ToggleSwitch } from "../components/ToggleSwitch";
import { useAstrology, 各n惑星の配置 } from "./カスタムフック";
import { 更新エリア } from "./更新エリア";
import { 閲覧エリア } from "./閲覧エリア";

import type { NextPage } from "next";
type 引数 = {
  各n惑星の配置?: 各n惑星の配置;
};

export const getServerSideProps: GetServerSideProps<引数> = async (
  コンテキスト
) => {
  if (typeof コンテキスト.query.q !== "string") {
    return {
      props: {},
    };
  }

  return {
    props: {
      // TODO: TypeGuard
      各n惑星の配置: JSON.parse(コンテキスト.query.q) as 各n惑星の配置,
    },
  };
};

export const ページ: NextPage<引数> = (引数) => {
  const {
    各n惑星の配置,
    各n惑星の配置を更新,
    各t惑星が各n惑星に対して取る各月のアスペクト,
  } = useAstrology(引数.各n惑星の配置);

  return (
    <>
      <Head>
        <title>2022年の各月のアスペクト</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <main className="p-4 pb-12 bg-white dark:bg-gray-800">
        <div className="max-w-screen-lg mx-auto">
          <div className="flex flex-wrap gap-4 items-center">
            <div className="flex-grow">
              <h1 className="mt-4 text-2xl text-black dark:text-gray-100">
                2022年の各月のアスペクト
              </h1>
            </div>

            <ToggleSwitch />
          </div>

          <更新エリア
            各n惑星の配置={各n惑星の配置}
            各n惑星の配置を更新={各n惑星の配置を更新}
          />

          <閲覧エリア
            各t惑星が各n惑星に対して取る各月のアスペクト={
              各t惑星が各n惑星に対して取る各月のアスペクト
            }
          />
        </div>
      </main>
    </>
  );
};
