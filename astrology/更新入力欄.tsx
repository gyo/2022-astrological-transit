import React from "react";

import {
  各n惑星の配置,
  各n惑星の配置を更新,
  星座である,
  星座一覧,
  配置,
} from "./カスタムフック";

type 引数 = {
  n惑星: keyof 各n惑星の配置;
  n惑星の配置: 配置;
  各n惑星の配置を更新: 各n惑星の配置を更新;
};

export const 更新入力欄: React.FC<引数> = (引数) => {
  const 星座の入力をハンドリング = React.useCallback(
    (イベント: React.ChangeEvent<HTMLSelectElement>) => {
      if (星座である(イベント.target.value)) {
        引数.各n惑星の配置を更新(引数.n惑星, { 星座: イベント.target.value });
      }
    },
    [引数]
  );

  const 度の入力をハンドリング = React.useCallback(
    (イベント: React.ChangeEvent<HTMLInputElement>) => {
      const 数値 = parseInt(イベント.target.value, 10);
      if (!isNaN(数値)) {
        引数.各n惑星の配置を更新(引数.n惑星, { 度: 数値 });
      }
    },
    [引数]
  );

  const 分の入力をハンドリング = React.useCallback(
    (イベント: React.ChangeEvent<HTMLInputElement>) => {
      const 数値 = parseInt(イベント.target.value, 10);
      if (!isNaN(数値)) {
        引数.各n惑星の配置を更新(引数.n惑星, { 分: 数値 });
      }
    },
    [引数]
  );

  return (
    <>
      <fieldset className="pt-4 pb-3">
        <div className="flex items-center">
          <legend className="font-bold w-14 text-black dark:text-gray-100">
            {引数.n惑星}
          </legend>
          <label className="mr-4">
            <span className="sr-only">星座</span>
            {/* eslint-disable-next-line jsx-a11y/no-onchange */}
            <select
              className="mr-1 border rounded border-gray-300 text-black bg-white dark:border-gray-600 dark:text-gray-100 dark:bg-gray-700"
              value={引数.n惑星の配置.星座}
              onChange={星座の入力をハンドリング}
            >
              {星座一覧.map((星座, インデックス) => {
                return (
                  <option value={星座} key={インデックス}>
                    {星座}
                  </option>
                );
              })}
            </select>
            <span className="text-black dark:text-gray-100">座</span>
          </label>
          <label className="mr-4">
            <span className="sr-only">度数</span>
            <input
              className="mr-1 w-10 border rounded border-gray-300 text-black bg-white dark:border-gray-600 dark:text-gray-100 dark:bg-gray-700"
              type="number"
              value={引数.n惑星の配置.度}
              onChange={度の入力をハンドリング}
              min="0"
              max="29"
            />
            <span className="text-black dark:text-gray-100">度</span>
          </label>
          <label className="mr-4">
            <span className="sr-only">分数</span>
            <input
              className="mr-1 w-10 border rounded border-gray-300 text-black bg-white dark:border-gray-600 dark:text-gray-100 dark:bg-gray-700"
              type="number"
              value={引数.n惑星の配置.分}
              onChange={分の入力をハンドリング}
              min="0"
              max="59"
            />
            <span className="text-black dark:text-gray-100">分</span>
          </label>
        </div>
      </fieldset>
    </>
  );
};
