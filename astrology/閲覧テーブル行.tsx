import React from "react";

import {
  アスペクトを表示用フォーマットに変換する,
  分数差を表示用フォーマットに変換する,
  特定のt惑星が特定のn惑星に対して取る各月のアスペクト,
} from "./カスタムフック";

type 引数 = {
  タイトル: string;
  特定のt惑星が特定のn惑星に対して取る各月のアスペクト: 特定のt惑星が特定のn惑星に対して取る各月のアスペクト;
};

export const 閲覧テーブル行: React.FC<引数> = (引数) => {
  return (
    <div className="flex">
      <div className="flex-shrink-0 grid sticky left-0 place-items-center h-14 w-16 py-1 border-b border-gray-300 dark:border-gray-500 text-black dark:text-gray-100 bg-white dark:bg-gray-800">
        {引数.タイトル}
      </div>
      {引数.特定のt惑星が特定のn惑星に対して取る各月のアスペクト.map(
        (特定のt惑星が特定のn惑星に対して取るアスペクト, インデックス) => {
          return (
            <React.Fragment key={インデックス}>
              <div className="flex-shrink-0 grid place-items-center w-3 py-1 border-b border-gray-300 dark:border-gray-500"></div>

              <div className="flex-shrink-0 grid place-items-center h-14 w-16 py-1 border-b border-gray-300 dark:border-gray-500 text-black dark:text-gray-100">
                {特定のt惑星が特定のn惑星に対して取るアスペクト != null ? (
                  <>
                    <span
                      className={`text-sm${
                        特定のt惑星が特定のn惑星に対して取るアスペクト.アスペクト ===
                          "コンジャンクション" ||
                        特定のt惑星が特定のn惑星に対して取るアスペクト.アスペクト ===
                          "オポジション" ||
                        特定のt惑星が特定のn惑星に対して取るアスペクト.アスペクト ===
                          "スクエア"
                          ? " text-red-500 dark:text-red-400"
                          : ""
                      }`}
                    >
                      {アスペクトを表示用フォーマットに変換する(
                        特定のt惑星が特定のn惑星に対して取るアスペクト.アスペクト
                      )}
                    </span>
                    <span
                      className={`${
                        特定のt惑星が特定のn惑星に対して取るアスペクト.分数差 <=
                        180
                          ? "font-bold"
                          : ""
                      }${
                        特定のt惑星が特定のn惑星に対して取るアスペクト.アスペクト ===
                          "コンジャンクション" ||
                        特定のt惑星が特定のn惑星に対して取るアスペクト.アスペクト ===
                          "オポジション" ||
                        特定のt惑星が特定のn惑星に対して取るアスペクト.アスペクト ===
                          "スクエア"
                          ? " text-red-500 dark:text-red-400"
                          : ""
                      }`}
                    >
                      {分数差を表示用フォーマットに変換する(
                        特定のt惑星が特定のn惑星に対して取るアスペクト.分数差
                      )}
                    </span>
                  </>
                ) : null}
              </div>
            </React.Fragment>
          );
        }
      )}
    </div>
  );
};
