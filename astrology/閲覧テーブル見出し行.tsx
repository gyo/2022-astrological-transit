import React from "react";

export const 閲覧テーブル見出し行: React.FC = () => {
  return (
    <div className="flex">
      <div className="flex-shrink-0 sticky left-0 grid place-items-center w-16 py-1 border-b border-gray-300 dark:border-gray-500 bg-white dark:bg-gray-800"></div>
      {Array(12)
        .fill(undefined)
        .map((_, インデックス) => {
          return (
            <React.Fragment key={インデックス}>
              <div className="flex-shrink-0 grid place-items-center w-3 py-1 border-b border-gray-300 dark:border-gray-500"></div>
              <div className="flex-shrink-0 grid place-items-center w-16 py-1 border-b border-gray-300 dark:border-gray-500 text-black dark:text-gray-100">
                {インデックス + 1}月
              </div>
            </React.Fragment>
          );
        })}
    </div>
  );
};
